/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.datevid.wrapperjxl;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.write.Number;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author @datevid
 * ver mas en: https://www.vogella.com/tutorials/JavaExcel/article.html
 */
public class WrapperJXL {
    private WritableCellFormat cellFormatBody;
    private WritableCellFormat cellFormatHeader;
    private String inputFile;

    // Initializing employees data to insert into the excel file
    public static List<Employee2> getData() {
        List<Employee2> employees =  new ArrayList<>();

        Calendar dateOfBirth = Calendar.getInstance();
        dateOfBirth.set(1992, 7, 21);
        employees.add(new Employee2(null, "rajeev@example.com",dateOfBirth.getTime(), 1200000.0));

        dateOfBirth.set(1965, 10, 15);
        employees.add(new Employee2("Thomas cook", "thomas@example.com",dateOfBirth.getTime(), 1500000.0));

        dateOfBirth.set(1987, 4, 18);
        employees.add(new Employee2("Steve Maiden", "steve@example.com",dateOfBirth.getTime(), 1800000.0));

        employees.add(new Employee2("David León", "datevid@gmail.com", new Date(), 150, "2020-05-06 16:24"));

        return employees;
    }

    public void setOutputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public byte[] write(List<Employee2> empleados,String[] headers) throws IOException, WriteException {

        /*FileOutputStream fileOut = new FileOutputStream("abc.xls");
        fileOut.write(file);
        fileOut.close();*/

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        //File file = new File(inputFile);
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));

        WritableWorkbook workbook = Workbook.createWorkbook(baos, wbSettings);
        workbook.createSheet("Report", 0);
        WritableSheet excelSheet = workbook.getSheet(0);
        createHeader(excelSheet,headers);
        createContent(excelSheet,empleados);

        workbook.write();
        workbook.close();

        byte[] xls = baos.toByteArray();
        return xls;
    }

    //set header and define cell format
    private void createHeader(WritableSheet sheet,String[] headers) throws WriteException {

        // Lets create a writableFontBody font
        WritableFont writableFontBody = new WritableFont(WritableFont.TAHOMA, 10);
        cellFormatBody = new WritableCellFormat(writableFontBody);
        // Lets automatically wrap the cells
        //cellFormatBody.setWrap(true);

        // create create a bold font with unterlines
        WritableFont writableFontHeader;
        writableFontHeader = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.DARK_BLUE);
        cellFormatHeader = new WritableCellFormat(writableFontHeader);
        // Lets automatically wrap the cells
        //cellFormatBody.setWrap(true);

        for (int i = 0; i < headers.length; i++) {
            addHeader(sheet, i, 0, headers[i]);
        }



    }

    private void createContent(WritableSheet sheet, List<Employee2> empleados) throws WriteException, RowsExceededException {
        for (int i = 0; i < empleados.size(); i++) {
            //N
            addNumber(sheet, 0, i+1, i+1);

            //Nombres
            addText(sheet, 1, i+1, empleados.get(i).getName());

            //Email
            addText(sheet, 2, i+1, empleados.get(i).getEmail());

            //Fecha Nacimiento
            //addDate(sheet, 3, i+1, empleados.get(i).getDateOfBirth());

            //Salario
            addNumber(sheet, 4, i+1, empleados.get(i).getSalary());

            //Ingreso Institucion
            addText(sheet, 5, i+1, empleados.get(i).getDateString());
        }

    }

    //see https://stackoverflow.com/q/12576980
    private void addDate(WritableSheet sheet, int column, int row, Date date)throws WriteException, RowsExceededException {
        DateFormat customDateFormat = new DateFormat ("yyyy-MM-dd hh:mm:ss");
        WritableCellFormat dateFormat = new WritableCellFormat (customDateFormat);
        //dateFormat.setWrap(true);
        DateTime dateTime = new DateTime(column, row, date, dateFormat);
        sheet.addCell(dateTime);

        //CellView cv = sheet.getColumnView(column);
        ////cv.setAutosize(true);
        //cv.setSize(100);
        //sheet.setColumnView(column, cv);

    }

    private void addHeader(WritableSheet sheet, int column, int row, String s)throws RowsExceededException, WriteException {

        CellView cv = new CellView();
        cv.setFormat(cellFormatHeader);
        cv.setAutosize(true);
        //cv.setSize(750);
        sheet.setColumnView(column,cv);

        Label label;
        label = new Label(column, row, s);
        sheet.addCell(label);
    }

    private void addNumber(WritableSheet sheet, int column, int row,Integer integer) throws WriteException, RowsExceededException {
        jxl.write.Number number;
        number = new Number(column, row, integer, cellFormatBody);
        sheet.addCell(number);
    }

    private void addNumber(WritableSheet sheet, int column, int row,Double aDouble) throws WriteException, RowsExceededException {
        jxl.write.Number number;
        number = new Number(column, row, aDouble, cellFormatBody);
        sheet.addCell(number);
    }

    private void addText(WritableSheet sheet, int column, int row, String s)throws WriteException, RowsExceededException {
        Label label;
        label = new Label(column, row, s, cellFormatBody);
        sheet.addCell(label);
    }

    public static void saveFile(byte[] file, String name) throws IOException {
        Date date = Calendar.getInstance().getTime();
        java.text.DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.hhmmss");
        String strDate = dateFormat.format(date);

        FileOutputStream fileOut = new FileOutputStream(name+strDate+".xls");
        fileOut.write(file);
        fileOut.close();

    }

    public static void main(String[] args) throws WriteException, IOException {

        String[] headers = {"Nº", "Nombres", "Email", "Fecha Nacimiento", "Salario","Ingreso Institucion"};
        List<Employee2> empleados =WrapperJXL.getData();
        String name = "reporteDocEmiXDest.xls";

        WrapperJXL test = new WrapperJXL();
        byte[] bytes = test.write(empleados,headers);
        WrapperJXL.saveFile(bytes,name);
        System.out.println("Please check the result file "+name);
    }
}
